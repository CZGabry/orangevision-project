<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Ignis
 */

get_header(); ?>
<style>
	.sticky-header .site-header {
		position: absolute
	}
	.header-text {
    background-color: #01adc8;
}
.post-thumbnail {
    display:none;
}
.entry-title {
    color: #fff!important;
    line-height: 500%;
}
.imgDiv {
    background-color: #fff;
    color: #000;
}
.imgDiv p {
    padding: 1em 2em;
}
#vocale {
    width: 100%;
}
#video {
    text-align: center;
}
.vocaleTesto {
    padding-top: 30px;
    width: 100%;
    height: auto;
    background-color: #fff;
}
.vocale {
    background-color: #fff;
}
nav.navigation,.post-navigation {
    display:none;
}
</style>
	<div id="primary" class="content-area col-md-10 nosidebar">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'single' );

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
