<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Ignis
 */

?>
		</div><!-- .container -->
	</div><!-- #content -->


	<footer id="colophon" class="site-footer clearfix" role="contentinfo">
		<div class="site-info">
			 <div class="col-sm-3" title="Articoli recenti">	
				 <h4 title="Articoli recenti">
					ARTICOLI RECENTI
				</h4>
				 <?php $the_query = new WP_Query( 'posts_per_page=3' ); ?>
 
<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
 
<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
 
<?php 
endwhile;
wp_reset_postdata();
?>
			</div>
			<div class="col-sm-3" title="Invia la tua storia">	
				<h4 title="Invia la tua storia">
					INVIA LA TUA STORIA
				</h4>
				 <a href="/index.php/invia-la-tua-storia/">
					 Raccontaci la tua storia
				 </a>
				<a href="/index.php/invia-la-tua-storia/">
					 Vuoi inviarci un vocale?
				 </a>
				<a href="/index.php/invia-la-tua-storia/">
					 Vuoi inviarci un vocale?
				 </a>
				
			</div>
			<div class="col-sm-3" title="Il Progetto di Armando">		
				<h4 title="Il progetto">
					IL PROGETTO
				</h4>
				 <a href="/index.php/progetto/">
					 Chi sono?
				 </a>
				<a href="/index.php/progetto/">
					 Alcune storie...
				 </a>
			</div>
			<div class="col-sm-3" title="I nostri social">		
				<h4 title="Social">
					SOCIAL
				</h4>
				<div>
					<p class="footer-twitter">
						Twitter
					</p>
					<p class="footer-facebook">
						Facebook
					</p>
					<p class="footer-instagram">
						Instagram
					</p>
				</div>
				 
			</div>
			<hr class="col-sm-12 display-none">
			<div class="col-sm-6 display-none">	
				<img src="/wp-content/uploads/2020/01/italia.png" title="Italia" alt="Bandiera italiana" style="float:left; border-radius: 5px">
				<p style="float:left; margin-left: 5px;  color: #fff; font-weight: bold">
					ITALIA
				</p>
			</div>
			<div class="col-sm-6 display-none">	
				<p style="float:right; font-style: italic; color: #fff">
					ARMANDO BORELLI
				</p>
			</div>
		</div>
<script> jQuery(document).ajaxComplete(function(){
       var span = document.querySelectorAll("SPAN.vc_gitem-post-category-name");
    console.log(span);
    for (var i = 0; i < span.length; i++) {
        console.log(span[i].innerText)

        console.log(2);


        switch (span[i].innerText) {
            case "Tecnologia":
                span[i].parentNode.classList.add("Tecnologia");
                break;
            case "Sport":
                span[i].parentNode.classList.add("Sport");
                break;
            case "Arte":
                span[i].parentNode.classList.add("Arte");
                break;
            case "Musica":
                span[i].parentNode.classList.add("Musica");
                break;
            case "Benessere":
                span[i].parentNode.classList.add("Benessere");
                break;
            case "Ironia":
                span[i].parentNode.classList.add("Ironia");
                break;
            default:
                break;
        }
    };
}); </script><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>