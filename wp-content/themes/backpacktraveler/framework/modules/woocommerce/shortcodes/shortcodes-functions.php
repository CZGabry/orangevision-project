<?php

if ( ! function_exists( 'backpacktraveler_mikado_include_woocommerce_shortcodes' ) ) {
	function backpacktraveler_mikado_include_woocommerce_shortcodes() {
		foreach ( glob( MIKADO_FRAMEWORK_MODULES_ROOT_DIR . '/woocommerce/shortcodes/*/load.php' ) as $shortcode_load ) {
			include_once $shortcode_load;
		}
	}
	
	if ( backpacktraveler_mikado_core_plugin_installed() ) {
		add_action( 'backpacktraveler_core_action_include_shortcodes_file', 'backpacktraveler_mikado_include_woocommerce_shortcodes' );
	}
}
