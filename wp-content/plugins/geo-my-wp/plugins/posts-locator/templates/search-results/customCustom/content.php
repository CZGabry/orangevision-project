<?php
/**
 * Posts locator "custom" search results template file. 
 * 
 * This file outputs the search results.
 * 
 * You can modify this file to apply custom changes. However, it is not recomended
 * since your changes will be overwritten on the next update of the plugin.
 * 
 * Instead you can copy-paste this template ( the "custom" folder contains this file 
 * and the "css" folder ) into the theme's or child theme's folder of your site 
 * and apply your changes from there. 
 * 
 * The template folder will need to be placed under:
 * your-theme's-or-child-theme's-folder/geo-my-wp/posts-locator/search-results/
 * 
 * Once the template folder is in the theme's folder you will be able to select 
 * it in the form editor. It will show in the "Search results" dropdown menu as "Custom: custom".
 *
 * @param $gmw  ( array ) the form being used
 * @param $gmw_form ( object ) the form object
 * @param $post ( object ) post object in the loop
 * 
 */
?>
<!--  Main results wrapper - wraps the paginations, map and results -->
<div class="gmw-results-wrapper custom <?php echo esc_attr( $gmw['prefix'] ); ?>" data-id="<?php echo absint( $gmw['ID'] ); ?>" data-prefix="<?php echo esc_attr( $gmw['prefix'] ); ?>">
	
	<?php if ( $gmw_form->has_locations() ) : ?>

        <div class="gmw-results">

			<?php do_action( 'gmw_search_results_start', $gmw ); ?>
			
			<div class="gmw-results-count">
				<span><?php gmw_results_message( $gmw ); ?></span>
				<?php do_action( 'gmw_search_results_after_results_message', $gmw ); ?>
			</div>
			
			<?php do_action( 'gmw_before_top_pagination', $gmw ); ?>
			
			<div class="pagination-per-page-wrapper top gmw-pt-pagination-wrapper gmw-pt-top-pagination-wrapper">
				<?php gmw_per_page( $gmw ); ?>
				<?php gmw_pagination( $gmw ); ?>
			</div> 
				
		    <?php gmw_results_map( $gmw ); ?>
			
			
			
			<?php do_action( 'gmw_search_results_before_loop', $gmw ); ?>
			
			<div class="vc_pageable-slide-wrapper">
				
				<?php while ( $gmw_query->have_posts() ) : $gmw_query->the_post(); ?>
					
					<?php global $post; ?>

					<div id="single-post-<?php echo absint( $post->ID ); ?>" class="vc_grid-item vc_clearfix vc_col-sm-6 vc_grid-item-zone-c-bottom vc_grid-term-5 vc_visible-item fadeIn animated">
    
					<?php do_action( 'gmw_search_results_loop_item_start', $gmw, $post ); ?><div class="vc_grid-item-mini vc_clearfix">
        <div class="vc_gitem-animated-block">
            <div class="vc_gitem-zone vc_gitem-zone-a post-post vc-gitem-zone-height-mode-auto vc-gitem-zone-height-mode-auto-1-1 " style="background-image: url('<?php 
	if ( has_post_thumbnail()) {
		$large_image_url = wp_get_attachment_image_url( get_post_thumbnail_id(), 'large');
		
		echo $large_image_url;
	 } else {
		echo "https://orange-vision.its2020.tk/wp-content/plugins/js_composer/assets/vc/vc_gitem_image.png";
	 }
	?>') !important; ">
						<?php do_action( 'gmw_search_results_before_excerpt', $gmw, $post ); ?>
						<div class="vc_gitem_row vc_row vc_gitem-row-position-top">
    <div class="vc_col-sm-4 vc_gitem-col vc_gitem-col-align-"></div>
    <div class="vc_col-sm-8 vc_gitem-col vc_gitem-col-align-">
        <div
            class="vc_gitem-post-data post-category vc_gitem-post-data-source-post_categories vc_grid-filter vc_clearfix vc_grid-filter-filled vc_grid-filter-filled-round-all vc_grid-filter-size-md vc_grid-filter-center vc_grid-filter-color-juicy_pink">
            <?php $category_detail=get_the_category($post->ID);
									foreach($category_detail as $cd){
										?>
            <div class="vc_grid-filter-item vc_gitem-post-category-name <?php
                    echo $cd->cat_name;
                    ?>"><span
                    class="vc_gitem-post-category-name"><?php
                    echo $cd->cat_name;
                    ?></span></div>
                    <?php } ?>
        </div>
    </div>
</div>
<div class="vc_gitem_row vc_row vc_gitem-row-position-bottom">
    <div class="vc_col-sm-8 vc_gitem-col vc_gitem-col-align- vc_custom_1579537756905">
        <div
            class="vc_gitem-post-data vc_custom_1579615523711 vc_gitem-post-data-source-post_categories vc_grid-filter vc_clearfix vc_grid-filter-filled vc_grid-filter-filled-round-all vc_grid-filter-size-md vc_grid-filter-center vc_grid-filter-color-juicy_pink left">
            
            <div class="vc_grid-filter-item post-location"><span
                    class="vc_gitem-post-category-name">
                    <?php gmw_search_results_address( $post, $gmw ); ?>
                    </span></div>
        </div>
    </div>
</div>
            </div>
		</div>
		<div class="vc_gitem-zone vc_gitem-zone-c ">
            <div class="vc_gitem-zone-mini ">
                <div class="vc_gitem_row vc_row vc_gitem-row-position-top ">
                    <div class="vc_col-sm-12 post-text vc_gitem-col vc_gitem-col-align- ">
                        <div class="vc_custom_heading post-title vc_gitem-post-data vc_gitem-post-data-source-post_title ">
						<h4 style="text-align: center">
						<?php the_title(); ?>
						</h4>
                        </div>
                        <div class="vc_btn3-container vc_btn3-center ">
                            <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-modern vc_btn3-color-turquoise " a=" " href="<?php echo the_permalink(); ?>" title="Scopri di più ">Scopri di più</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="vc_clearfix ">
	</div>
	<?php do_action( 'gmw_search_results_loop_item_end', $gmw, $post ); ?>
	
</div>
		   
			
				<?php endwhile; ?>
			
									</div>   
			
			<?php do_action( 'gmw_search_results_after_loop', $gmw ); ?>
			
			<div class="pagination-per-page-wrapper bottom gmw-pt-pagination-wrapper gmw-pt-bottom-pagination-wrapper">
				<?php gmw_per_page( $gmw ); ?>
				<?php gmw_pagination( $gmw ); ?>
			</div>

		</div>

	<?php else : ?>

        <div class="gmw-no-results">
            
            <?php do_action( 'gmw_no_results_start', $gmw ); ?>

            <?php gmw_no_results_message( $gmw ); ?>
            
            <?php do_action( 'gmw_no_results_end', $gmw ); ?> 

        </div>

    <?php endif; ?>
	
</div> 
